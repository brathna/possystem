﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class Product : IProduct
    {
        private readonly POSContext Context;
        private ApiResponse response;
        public Product(POSContext context, ApiResponse response)
        {
            Context = context;
            this.response = response;
        }

        //=========================================================
        //Create products
        //==========================================================
        public async Task<ApiResponse> CreateProduct(TblProducts pro)
        {
            try
            {
                if (pro.ProductNameEn != null && pro.ProductNameKh != null && pro.CateId > 0
                    && pro.Price > 0)
                {
                    Context.TblProducts.Add(pro);
                    await Context.SaveChangesAsync();
                    response.Message = "Sucess";
                    response.Code = 200;
                    response.Data = pro;
                }
                else
                {
                    response.Message = "Data can not null or zero";
                    response.Code = 401;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        //================================
        //GetProductByID
        //================================
        public async Task<ApiResponse> GetProductById(int id)
        {
            try
            {
                var pro = await Context.TblProducts.Where(x => x.ProId.Equals(id)).SingleOrDefaultAsync();

                if (pro != null)
                {
                    response.Data = pro;
                    response.Code = 200;
                    response.Message = "Get Success";
                    return response;
                }
                else
                {
                    response.Message = "Get Product By ID Failed Request";
                    response.Code = 401;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        //=========================================================
        //Delete products
        //==========================================================
        public async Task<ApiResponse> DeleteProduct(int id)
        {
            try
            {
                if (id > 0)
                {
                    var pro = await Context.TblProducts.Where(x => x.ProId.Equals(id)).SingleOrDefaultAsync();
                    if (pro != null)
                    {
                        pro.CateId = -1;
                        await Context.SaveChangesAsync();

                        response.Message = "success";
                        response.Code = 200;
                        return response;
                    }
                    else
                    {
                        response.Message = "Bad Request";
                        response.Code = 401;
                        return response;
                    }
                }
                else
                {
                    response.Code = 401;
                    response.Message = "Bad Request";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        //=========================================================
        //List products
        //==========================================================
        public async Task<ApiResponse> ListProduct(bool isSearch = false, string search = "")
        {
            try
            {
                if (isSearch)
                {
                    var pro = await Context.VwSelectAllProduct.Where(x => x.ProductNameEn.Contains(search) ||
                    x.ProductNameKh.Contains(search) || x.CategoryNameEn.Contains(search)).ToListAsync();
                    response.Data = pro;
                    response.Message = "Sucess";
                    response.Code = 200;
                }
                else
                {
                    var pro = await Context.VwSelectAllProduct.Where(x => x.Status == true).ToListAsync();
                    response.Data = pro;
                    response.Message = "Sucess";
                    response.Code = 200;
                }
                return response;
            }

            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        //=========================================================
        //Update products
        //==========================================================
        public async Task<ApiResponse> UpdateProduct(TblProducts product)
        {
            try
            {
                if (product != null && product.ProId > 0 && product.CateId > 0 && product.Price > 0)
                {
                    var pro = await Context.TblProducts.Where(x => x.ProId.Equals(product.ProId)).SingleOrDefaultAsync();
                    if (pro != null)
                    {
                        pro.CateId = product.CateId;
                        pro.ProductNameEn = product.ProductNameEn;
                        pro.ProductNameKh = product.ProductNameKh;
                        pro.Price = product.Price;
                        //pro.Size = product.Size;
                        pro.Status = product.Status;
                        pro.Url = product.Url;

                        await Context.SaveChangesAsync();
                        response.Message = "Sucess";
                        response.Code = 200;
                        response.Data = pro;
                        return response;
                    }
                    else
                    {
                        response.Code = 401;
                        response.Message = "Bad Request";
                        return response;
                    }
                }
                else
                {
                    response.Code = 401;
                    response.Message = "proId not much with data, " + "cateId, price, qty must greater than zero";
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> GetCategoryByList()
        {
            try
            {
                var pro = await Context.TblCategory.Where(x => x.Status == true).ToListAsync();
                response.Message = "Sucess";
                response.Code = 200;
                response.Data = pro;
                return response;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
