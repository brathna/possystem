﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class Category : ICategory
    {
        private readonly POSContext Context;
        private ApiResponse response;

        public Category(POSContext context, ApiResponse response)
        {
            Context = context;
            this.response = response;
        }

        public async Task<ApiResponse> CreateCategory(TblCategory cate)
        {
            try
            {
                if (!string.IsNullOrEmpty(cate.CategoryNameKh) && !string.IsNullOrEmpty(cate.CategoryNameKh))
                {
                    Context.TblCategory.Add(cate);
                    await Context.SaveChangesAsync();
                    response.Message = "Create Category Sucess";
                    response.Code = 200;
                    response.Data = cate;
                }
                else
                {
                    response.Message = "Category name can not null";
                    response.Code = 401;
                }
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> DeleteCategory(int id)
        {
            try
            {
                if (id > 0)
                {
                    var cate = await Context.TblCategory.Where(x => x.CateId.Equals(id)).SingleOrDefaultAsync();
                    if (cate != null)
                    {
                        cate.Status = false;
                        //Context.Remove(cate);
                        await Context.SaveChangesAsync();
                        await Context.SaveChangesAsync();
                        response.Message = "Category was deleted";
                        response.Code = 200;
                        return response;

                    }
                    else
                    {
                        response.Message = "Data Null Can not delete";
                        response.Code = 401;
                        return response;
                    }
                }
                else
                {
                    response.Message = "CateId must greater than zero";
                    response.Code = 401;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> GetCategoryById(int id)
        {
            try
            {
                var cate = await Context.TblCategory.Where(x => x.CateId == id).SingleOrDefaultAsync();
                //var cate = await Context.TblCategory.Where(x => x.CateId.Equals(id)).SingleOrDefaultAsync();
                if (cate != null)
                {
                    response.Data = cate;
                    response.Message = "Get Category By ID Success";
                    response.Code = 200;
                    return response;
                }
                else
                {
                    response.Message = "Category Get Failed";
                    response.Code = 401;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }

        public async Task<ApiResponse> ListCategory()
        {
            try
            {
                var category = await Context.TblCategory.Where(x => x.Status == true).ToListAsync();
                if (category != null)
                {
                    response.Message = "List Category Success";
                    response.Code = 200;
                    response.Data = category;
                    return response;
                }
                else
                {
                    response.Message = "Failed";
                    response.Code = 200;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> UpdateCategory(TblCategory category)
        {
            try
            {
                if (category != null && category.CateId > 0 && !string.IsNullOrEmpty(category.CategoryNameEn) && !string.IsNullOrEmpty(category.CategoryNameKh))
                {
                    var cate = Context.TblCategory.Where(x => x.CateId.Equals(category.CateId)).FirstOrDefault();
                    if (cate != null)
                    {
                        cate.CateId = category.CateId;
                        cate.CategoryNameEn = category.CategoryNameEn;
                        cate.CategoryNameKh = category.CategoryNameKh;

                        await Context.SaveChangesAsync();
                        response.Message = "Category Saved Success";
                        response.Code = 200;
                        response.Data = cate;
                        return response;
                    }
                    else
                    {
                        response.Message = "No Data, Category update failed ";
                        response.Code = 401;
                        return response;
                    }
                }
                else
                {
                    response.Message = "CateId null can't update";
                    response.Code = 401;
                    return response;
                }
            }
            catch (Exception ex)
            {
                response.Message = ex.Message;
                response.Code = 500;
                return response;
            }
        }
    }
}
