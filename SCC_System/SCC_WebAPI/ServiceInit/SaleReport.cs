﻿using Microsoft.EntityFrameworkCore;
using SaleModal.Classes;
using SaleModal.DB.Context;
using SaleModal.Services;

namespace SCC_WebAPI.ServiceInit
{
    public class SaleReport : ISaleReport
    {
        private readonly POSContext Context;
        private ApiResponse response;

        public SaleReport(POSContext context, ApiResponse response)
        {
            Context = context;
            this.response = response;
        }

        public async Task<ApiResponse> GetUserReport()
        {
            try
            {
                var result = await Context.VwGetUserReport.ToListAsync();
                response.Code = 200;
                response.Message = "success";
                response.Data = result;
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }

        public async Task<ApiResponse> ListSaleReport(SaleFilter filter)
        {
            try
            {
                var result = await Context.Procedures.sp_SaleReportAsync(filter.dateFrom, filter.dateTo, filter.SaleId);
                response.Code = 200;
                response.Message = "success";
                response.Data = result;
                return response;
            }
            catch (Exception ex)
            {
                response.Code = 500;
                response.Message = ex.Message;
                return response;
            }
        }
    }
}
