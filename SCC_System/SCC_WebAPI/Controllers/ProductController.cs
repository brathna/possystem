﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IProduct pro;
        public ProductController(IProduct pro)
        {
            this.pro = pro;
        }
        [HttpGet("ListProduct")]
        public async Task<ApiResponse> ListProduct(bool isSearch = false, string? search = "")
        {
            if (isSearch)
            {
                return await pro.ListProduct(isSearch, search);
            }
            else
            {
                return await pro.ListProduct(isSearch, search);
            }


        }
        [HttpGet("GetCategoryByList")]
        public async Task<ApiResponse> GetCategoryByList()
        {
            return await pro.GetCategoryByList();
        }
        [HttpPost("CreateProduct")]
        public async Task<ApiResponse> CreateProduct(TblProducts product)
        {
            return await pro.CreateProduct(product);
        }
        [HttpPost("UpdateProduct")]
        public async Task<ApiResponse> UpdateProduct(TblProducts product)
        {
            return await pro.UpdateProduct(product);
        }
        [HttpGet("DeleteProduct/{id:int}")]
        public async Task<ApiResponse> DeleteProduct(int id)
        {
            return await pro.DeleteProduct(id);
        }
        [HttpGet("GetProductById/{id:int}")]
        public async Task<ApiResponse> GetProductById(int id)
        {
            return await pro.GetProductById(id);
        }
    }
}
