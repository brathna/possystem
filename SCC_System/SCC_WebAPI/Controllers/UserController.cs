﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class UserController : Controller
    {
        private readonly IUser users;

        public UserController(IUser user)
        {
            this.users = user;
        }

        [HttpGet("ListUser")]
        public async Task<ApiResponse> Listuser()
        {
            return await users.ListUser();
        }
        [HttpPost("CreateUser")]
        public async Task<ApiResponse> Createuser(TblUser user)
        {
            return await users.CreateUser(user);
        }
        [HttpPost("UpdateUser")]
        public async Task<ApiResponse> Updateuser(TblUser userup)
        {
            return await users.UpdateUser(userup);
        }
        [HttpGet("GetUserById/{id:int}")]
        public async Task<ApiResponse> GetuserById(int id)
        {
            return await users.GetUserById(id);
        }
        [HttpGet("GetRoleByList")]
        public async Task<ApiResponse> GetRoleByList()
        {
            return await users.GetRoleByList();
        }

        [HttpGet("GetEmployeeList")]
        public async Task<ApiResponse> GetUserList()
        {
            return await users.GetEmployeeList();
        }

        [HttpGet("DeleteUser/{id:int}")]
        public async Task<ApiResponse> DeleteUser(int id)
        {
            return await users.DeleteUser(id);
        }
    }
}
