﻿
using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class SaleReportController : Controller
    {
        private readonly ISaleReport salereport;
        public SaleReportController(ISaleReport salereport)
        {
            this.salereport = salereport;
        }
        [HttpPost("ListSaleReport")]
        public async Task<ApiResponse> ListSaleReport(SaleFilter filter)
        {
            return await salereport.ListSaleReport(filter);
        }
        [HttpGet("GetUserReport")]
        public async Task<ApiResponse> GetUserReport()
        {
            return await salereport.GetUserReport();
        }
    }
}
