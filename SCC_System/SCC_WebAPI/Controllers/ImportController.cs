﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.Services;


namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImportController : Controller
    {
        private readonly IImport import;
        public ImportController(IImport import)
        {
            this.import = import;
        }

        [HttpGet("ListImport")]
        public async Task<ApiResponse> ListImport()
        {
            return await import.ListImport();
        }

        [HttpGet("GetImportDetail/{id:int}")]
        public async Task<ApiResponse> GetImportDetail(int importId)
        {
            return await import.GetImportDetail(importId);
        }

        [HttpPost("CreateImport")]
        public async Task<ApiResponse> CreateImport(ImportPost importPost)
        {
            return await this.import.CreateImport(importPost);
        }

    }
}
