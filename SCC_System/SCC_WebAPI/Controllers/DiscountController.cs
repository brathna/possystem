﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DiscountController : Controller
    {
        private readonly IDiscount discount;
        public DiscountController(IDiscount discount)
        {
            this.discount = discount;
        }

        [HttpGet("ListDiscount")]
        public async Task<ApiResponse> ListDiscount()
        {
            return await discount.ListDiscount();
        }

        [HttpPost("CreateDiscount")]
        public async Task<ApiResponse> CreateDiscount(TblDiscount newDisc)
        {
            return await discount.CreateDiscount(newDisc);
        }

        [HttpGet("GetDiscountById")]
        public async Task<ApiResponse> GetDiscountById(int id)
        {
            return await discount.GetDiscountById(id);
        }

        [HttpPost("UpdateDiscount")]
        public async Task<ApiResponse> UpdateDiscount(TblDiscount upDiscount)
        {
            return await discount.UpdateDiscount(upDiscount);
        }

        [HttpGet("DeleteDiscount")]
        public async Task<ApiResponse> DeleteDiscount(int id)
        {
            return await discount.DeleteDiscount(id);
        }
    }
}
