﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class SalesController : Controller
    {
        private readonly ISales sale;
        public SalesController(ISales sale)
        {
            this.sale = sale;
        }

        [HttpGet("ListSale")]
        public async Task<ApiResponse> ListSales()
        {
            return await sale.ListSales();
        }

        [HttpGet("GetSaleDetail")]
        public async Task<ApiResponse> GetSaleDetail(int saleId)
        {
            return await sale.GetSaleDetail(saleId);
        }

        [HttpGet("GetListProduct")]
        public async Task<ApiResponse> GetListProduct()
        {
            return await sale.GetListProduct();
        }

        [HttpGet("GetListDiscount")]
        public async Task<ApiResponse> GetDiscount()
        {
            return await sale.GetListDiscount();
        }

        [HttpGet("GetListExchangeRate")]
        public async Task<ApiResponse> GetExchangeRate()
        {
            return await sale.GetListExchangeRate();
        }

        //============================
        //create
        //============================
        [HttpPost("CreateSale")]
        public async Task<ApiResponse> CreateSale(SalePost post)
        {
            return await sale.CreateSale(post);
        }

        [HttpGet("GetOrderDetail")]
        public async Task<ApiResponse> GetOrderDetail()
        {
            return await sale.GetOrderDetail();
        }
    }
}
