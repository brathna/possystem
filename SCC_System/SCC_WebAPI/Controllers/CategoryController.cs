﻿using Microsoft.AspNetCore.Mvc;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    //[Authorize]

    public class CategoryController : Controller
    {

        private readonly ICategory cate;                                       //param of ICategory Controller 
        public CategoryController(ICategory category)                   //param of ICategory 
        {
            this.cate = category;
        }
        [HttpGet("ListCategory")]
        public async Task<ApiResponse> ListCategory()
        {
            return await cate.ListCategory();
        }

        [HttpPost("CreateCategory")]
        public async Task<ApiResponse> CreateCategory(TblCategory category)
        {
            return await cate.CreateCategory(category);
        }

        [HttpGet("DeleteCategory/{id:int}")]
        public async Task<ApiResponse> DeleteCategory(int id)
        {
            return await cate.DeleteCategory(id);
        }

        [HttpGet("GetCategoryById/{id:int}")]
        public async Task<ApiResponse> GetCategoryById(int id)
        {
            return await cate.GetCategoryById(id);
        }

        [HttpPost("UpdateCategory")]
        public async Task<ApiResponse> UpdateCategory(TblCategory category)
        {
            return await cate.UpdateCategory(category);
        }
    }
}
