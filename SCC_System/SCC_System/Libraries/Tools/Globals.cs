﻿namespace SCC_System.Libraries.Tools
{
    public static class Globals
    {
        //public static List<TblMenu>? MENUS { get; set; }

        public static HttpClient? CLIENT { get; set; }
        public static string? USERNAME { get; set; }
        public static string? EMPLOYEE_ID { get; set; }
        public static string? USER_ROLE { get; set; }
        public static string? DIVISION_ID { get; set; }
        public static string VERSION { get; set; } = "ជំនាន់ V1.0.3";
    }
}
