﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class SaleReport : ISaleReport
    {
        private readonly HttpClient Client;
        public SaleReport(HttpClient httpclient)
        {
            this.Client = httpclient;
        }
        public async Task<ApiResponse> GetUserReport()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/SaleReport/GetUserReport");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<ApiResponse> ListSaleReport(SaleFilter filter)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/SaleReport/ListSaleReport", filter);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
