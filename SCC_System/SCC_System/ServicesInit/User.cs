﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class User : IUser
    {
        private readonly HttpClient Client;
        public User(HttpClient httpclient)
        {
            this.Client = httpclient;
        }

        public async Task<ApiResponse> CreateUser(TblUser use)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/User/CreateUser", use);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> DeleteUser(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/User/DeleteUser/" + id);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> GetUserById(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/User/GetUserById/" + id);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> ListUser()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/User/ListUser");
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<ApiResponse> GetRoleByList()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/User/GetRoleByList");
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ApiResponse> GetEmployeeList()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/User/GetEmployeeList");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> UpdateUser(TblUser user)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/User/UpdateUser/", user);
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}
