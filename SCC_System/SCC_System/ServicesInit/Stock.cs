﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class Stock : IStock
    {
        private readonly HttpClient Client;
        public Stock(HttpClient httpclient)
        {
            this.Client = httpclient;
        }

        public async Task<ApiResponse> CreateStock(TblStock st)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Stock/CreateStock", st);
                return result;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<ApiResponse> ListStock()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Stock/ListStock");
                return result;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<ApiResponse> ListStockOut()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Stock/ListStockOut");
                return result;
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<ApiResponse> OutStock(TblStockOut stout)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Stock/OutStock", stout);
                return result;
            }
            catch (Exception e)
            {
                throw;
            }
        }

    }
}
