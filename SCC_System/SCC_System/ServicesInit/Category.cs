﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class Category : ICategory
    {
        private readonly HttpClient Client;
        public Category(HttpClient HttpClient)
        {
            Client = HttpClient;
        }

        public async Task<ApiResponse> CreateCategory(TblCategory cate)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Category/CreateCategory", cate);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> DeleteCategory(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Category/DeleteCategory/" + id);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> GetCategoryById(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Category/GetCategoryById/" + id);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> ListCategory()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Category/ListCategory");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> UpdateCategory(TblCategory category)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Category/UpdateCategory", category);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
