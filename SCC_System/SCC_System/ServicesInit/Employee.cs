﻿
using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class Employee : IEmployee
    {
        private readonly HttpClient Client;
        public Employee(HttpClient httpClient)
        {
            this.Client = httpClient;
        }
        public async Task<ApiResponse> CreateEmployee(TblEmployee emp)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Employee/CreateEmployee", emp);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> DeleteEmployee(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Employee/DeleteEmployee/"+id);
                return result;
            }
            catch (Exception ex)
            {
                throw; 
            }
        }

        public async Task<ApiResponse> GetEmployeeById(int id)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Employee/GetEmployeeById/" + id);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<ApiResponse>  ListEmployee()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Employee/Listemployee");
                return result;
            }
            catch (Exception ex)
            {
                throw ;
            }
        }

        public async Task<ApiResponse> UpdateEmployee(TblEmployee employee)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Employee/UpdateEmployee",employee);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
