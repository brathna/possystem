﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class Sales : ISales
    {
        private readonly HttpClient Client;
        public Sales(HttpClient httpClient)
        {
            this.Client = httpClient;
        }
        public async Task<ApiResponse> CreateSale(SalePost post)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Sales/CreateSale", post);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> GetListDiscount()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Sales/GetListDiscount");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> GetListExchangeRate()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Sales/GetListExchangeRate");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> GetListProduct()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Sales/GetListProduct");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<ApiResponse> GetOrderDetail()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Sales/GetOrderDetail");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> GetSaleDetail(int saleId)
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Sales/GetSaleDetail" + saleId);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> ListSales()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Sales/ListSale");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
