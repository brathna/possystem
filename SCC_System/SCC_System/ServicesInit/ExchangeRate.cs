﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class ExchangeRate : IExchangeRate
    {
        private readonly HttpClient Client;
        public ExchangeRate(HttpClient HttpClient)
        {
            Client = HttpClient;
        }
        public async Task<ApiResponse> CreateExchangeRate(TblExchangeRate exchange)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/ExchangeRate/CreateExchangeRate", exchange);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ApiResponse> GetExchangeRateByDate()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/ExchangeRate/Search_date");
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ApiResponse> ListExchangeRate()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/ExchangeRate/ListExchangeRate");
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ApiResponse> UpdateExchangeRate(TblExchangeRate exchangerate)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/ExchangeRate/UpdateExchangeRate", exchangerate);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> getExchangeById(int id)
        {
            var result = await Client.GetJsonAsync<ApiResponse>("api/exchangerate/id?id=" + id);
            return result;
        }

        public async Task<ApiResponse> DeleteRate(int rateId)
        {
            var result = await Client.GetJsonAsync<ApiResponse>("api/exchangerate/DeleteExchangeRate?id=" + rateId);
            return result;
        }
    }
}
