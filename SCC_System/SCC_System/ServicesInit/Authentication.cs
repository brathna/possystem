﻿using Microsoft.AspNetCore.Components;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.ServicesInit
{
    public class Authentication : IAuthentication
    {
        private readonly HttpClient Client;
        public Authentication(HttpClient client)
        {
            Client = client;
        }

        public async Task<ApiResponse> AuthAccess(TblUser users)
        {
            try
            {
                var result = await Client.PostJsonAsync<ApiResponse>("api/Authentication/AuthAccess", users);
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> GetRolePermission()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Authentication/RolePermission");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<ApiResponse> ListUser()
        {
            try
            {
                var result = await Client.GetJsonAsync<ApiResponse>("api/Authentication/UserList");
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public async Task<ApiResponse> LoginUser(VwUserMenuRole m)
        {
            throw new NotImplementedException();
        }
    }
}
