﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class RoleResponse
    {
        private readonly IRolePermission RolePer;
        ApiResponse response;
        public TblRolePermission? rolePermission { get; set; }
        public List<VwGetRolePermission> vwgetRolePer { get; set; }
        public List<TblPermission> permissionLists { get; set; }
        public List<TblSubPermission> SubLists = new List<TblSubPermission>();

        public RoleResponse(IRolePermission roleper)
        {
            RolePer = roleper;
        }

        public async Task<List<VwGetRolePermission>> ListRolePermission()
        {
            vwgetRolePer = new List<VwGetRolePermission>();
            try
            {
                response = await RolePer.ListRolePermission();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        vwgetRolePer = JsonConvert.DeserializeObject<List<VwGetRolePermission>>(Data);
                    }
                }
                return vwgetRolePer;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<TblRolePermission> GetRolePerById(int id)
        {
            try
            {
                response = await this.RolePer.GetRolePerById(id);
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        rolePermission = JsonConvert.DeserializeObject<TblRolePermission>(Data);
                    }
                }
                return rolePermission;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<TblSubPermission>> GetSubPremission()
        {
            try
            {
                response = await this.RolePer.GetSubPermissionList();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        SubLists = JsonConvert.DeserializeObject<List<TblSubPermission>>(Data);
                    }
                }
                return SubLists;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<TblPermission>> GetPermissionList()
        {
            permissionLists = new List<TblPermission>();
            try
            {
                response = await this.RolePer.GetPermByList();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        permissionLists = JsonConvert.DeserializeObject<List<TblPermission>>(Data);
                    }
                }
                return permissionLists;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task AddRolePermission(TblRolePermission roleper)
        {
            try
            {
                response = await this.RolePer.CreateRolePermission(roleper);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task EditRolePermission(TblRolePermission rolePermission)
        {
            try
            {
                response = await this.RolePer.UpdateRolePermission(rolePermission);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task DeleteRolePermission(int id)
        {
            try
            {
                response = await this.RolePer.DeleteRolePermission(id);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}
