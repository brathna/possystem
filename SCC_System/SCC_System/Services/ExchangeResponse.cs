﻿
using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class ExchangeResponse
    {
        private readonly IExchangeRate exchange;
        public List<TblExchangeRate> listExchange { get; set; }
        public TblExchangeRate? exRate { get; set; }
        ApiResponse response;

        public ExchangeResponse(IExchangeRate exchangeRate)
        {
            exchange = exchangeRate;
        }


        public async Task<List<TblExchangeRate>> ListExchangeRate()
        {
            listExchange = new List<TblExchangeRate>();
            try
            {
                response = await exchange.ListExchangeRate();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        listExchange = JsonConvert.DeserializeObject<List<TblExchangeRate>>(Data);
                    }
                }
                return listExchange;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task AddExchangeRate(TblExchangeRate exchange)
        {
            try
            {
                response = await this.exchange.CreateExchangeRate(exchange);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task EditExchangeRate(TblExchangeRate exchangeRate)
        {
            try
            {
                response = await this.exchange.UpdateExchangeRate(exchangeRate);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<TblExchangeRate> GetExchangeRateByDate()
        {
            try
            {
                response = await exchange.GetExchangeRateByDate();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        exRate = JsonConvert.DeserializeObject<TblExchangeRate>(Data);
                    }
                }
                return exRate;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<TblExchangeRate> getExchangeById(int id)
        {
            try
            {
                ApiResponse response = await this.exchange.getExchangeById(id);
                if (response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        exRate = JsonConvert.DeserializeObject<TblExchangeRate>(Data);
                    }
                }
                return exRate;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task deletedExchangeRate(int rateId)
        {
            try
            {
                response = await this.exchange.DeleteRate(rateId);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
