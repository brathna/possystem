﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class ProductResponse
    {
        private readonly IProduct Pro;
        public List<VwSelectAllProduct> ProductList { get; set; }
        public List<TblCategory> CategoryList { get; set; }
        public TblProducts? Product { get; set; }
        ApiResponse response;

        public ProductResponse(IProduct pro)
        {
            Pro = pro;
        }
        public async Task<List<VwSelectAllProduct>> ProductsList(bool isSearch = false, string? search = "")
        {
            ProductList = new List<VwSelectAllProduct>();
            try
            {
                response = await Pro.ListProduct(isSearch, search);
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        ProductList = JsonConvert.DeserializeObject<List<VwSelectAllProduct>>(Data);
                    }
                }
                return ProductList;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        //=======================================================
        //GetProductById
        //=======================================================
        public async Task<TblProducts> GetProductById(int id)
        {
            try
            {
                response = await this.Pro.GetProductById(id);
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        Product = JsonConvert.DeserializeObject<TblProducts>(Data);
                    }
                }
                return Product;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        //===================================
        //GetCategoryList
        //===================================
        public async Task<List<TblCategory>> GetCategoryByList()
        {
            CategoryList = new List<TblCategory>();
            try
            {
                response = await this.Pro.GetCategoryByList();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        CategoryList = JsonConvert.DeserializeObject<List<TblCategory>>(Data);
                    }
                }
                return CategoryList;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //===================================
        //Update Product(save)
        //===================================
        public async Task EditProduct(TblProducts pro)
        {
            try
            {
                response = await this.Pro.UpdateProduct(pro);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //=================================
        //Delete Product
        //=================================
        public async Task DeleteProduct(int id)
        {
            try
            {
                response = await this.Pro.DeleteProduct(id);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        //=================================
        //Create Product
        //=================================
        public async Task AddNewProduct(TblProducts pro)
        {
            try
            {
                response = await this.Pro.CreateProduct(pro);
            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }
}
