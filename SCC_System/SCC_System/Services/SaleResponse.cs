﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class SaleResponse
    {
        private readonly ISales sale;
        ApiResponse response;
        public List<VwSelectAllProduct> ProductList { get; set; }
        public List<VwGetOrderById> listOrder = new List<VwGetOrderById>();
        public List<TblDiscount> listDiscount = new List<TblDiscount>();
        public List<TblExchangeRate> ListRate = new List<TblExchangeRate>();

        public SaleResponse(ISales sales)
        {
            this.sale = sales;
        }

        public async Task<List<TblExchangeRate>> GetExchangRate()
        {
            try
            {
                response = await this.sale.GetListExchangeRate();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        ListRate = JsonConvert.DeserializeObject<List<TblExchangeRate>>(Data);
                    }
                }
                return ListRate;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<List<TblDiscount>> GetListDiscount()
        {
            try
            {
                response = await this.sale.GetListDiscount();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        listDiscount = JsonConvert.DeserializeObject<List<TblDiscount>>(Data);
                    }
                }
                return listDiscount;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public async Task<List<VwSelectAllProduct>> GetProducts()
        {
            try
            {
                response = await this.sale.GetListProduct();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        ProductList = JsonConvert.DeserializeObject<List<VwSelectAllProduct>>(Data);
                    }
                }
                return ProductList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<ApiResponse> AddSale(SalePost post)
        {
            try
            {
                response = await this.sale.CreateSale(post);
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<VwGetOrderById>> GetOrderlist()
        {
            try
            {
                response = await this.sale.GetOrderDetail();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        listOrder = JsonConvert.DeserializeObject<List<VwGetOrderById>>(Data);
                    }
                }
                return listOrder;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public async Task<List<VwGetOrderById>> GetSaleDetail(int saleId)
        {
            try
            {
                response = await this.sale.GetSaleDetail(saleId);
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        listOrder = JsonConvert.DeserializeObject<List<VwGetOrderById>>(Data);
                    }
                }
                return listOrder;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<VwGetOrderById>> ListOrder()
        {
            try
            {
                response = await this.sale.ListSales();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        listOrder = JsonConvert.DeserializeObject<List<VwGetOrderById>>(Data);
                    }
                }
                return listOrder;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
