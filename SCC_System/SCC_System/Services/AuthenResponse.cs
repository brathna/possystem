﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class AuthenResponse
    {
        private readonly IAuthentication auth;
        ApiResponse response;

        public List<TblUser> userlist = new List<TblUser>();
        public TblUser tmpUser = new TblUser();
        public List<TblRolePermission> role = new List<TblRolePermission>();
        public AuthenResponse(IAuthentication auth)
        {
            this.auth = auth;
        }

        public async Task<List<TblUser>> UserLists()
        {
            try
            {
                response = await auth.ListUser();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        userlist = JsonConvert.DeserializeObject<List<TblUser>>(Data);
                    }
                }
                return userlist;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<List<TblRolePermission>> GetRolePermission()
        {
            try
            {
                response = await auth.GetRolePermission();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        role = JsonConvert.DeserializeObject<List<TblRolePermission>>(Data);
                    }
                }
                return role;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task<TblUser> AuthAccess(TblUser user)
        {
            try
            {
                var resp = await this.auth.AuthAccess(user);
                if (resp.Code == 200 && resp.Data != null)
                {
                    string? data = resp.Data.ToString();
                    if (data != null)
                    {
                        tmpUser = JsonConvert.DeserializeObject<TblUser>(data);
                    }
                }
                return tmpUser;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
