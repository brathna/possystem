﻿using Newtonsoft.Json;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SaleModal.Services;

namespace SCC_System.Services
{
    public class DiscountResponse
    {
        private readonly IDiscount Disc;
        ApiResponse response;
        public List<TblDiscount> disco = new List<TblDiscount>();
        public TblDiscount discount = new TblDiscount();
        public DiscountResponse(IDiscount disc)
        {
            Disc = disc;
        }

        public async Task<List<TblDiscount>> GetListDiscount()
        {
            try
            {
                response = await Disc.ListDiscount();
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        disco = JsonConvert.DeserializeObject<List<TblDiscount>>(Data);
                    }
                }
                return disco;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task NewDiscount(TblDiscount creDis)
        {
            try
            {
                response = await this.Disc.CreateDiscount(creDis);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task DeleteDiscount(int delDisco)
        {
            try
            {
                response = await this.Disc.DeleteDiscount(delDisco);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task UpdateDiscount(TblDiscount upDisco)
        {
            try
            {
                response = await this.Disc.UpdateDiscount(upDisco);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public async Task<TblDiscount> GetDiscountById(int id)
        {
            try
            {
                response = await this.Disc.GetDiscountById(id);
                if (response.Code == 200 && response.Data != null)
                {
                    string? Data = response.Data.ToString();
                    if (Data != null)
                    {
                        discount = JsonConvert.DeserializeObject<TblDiscount>(Data);
                    }
                }
                return discount;
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
