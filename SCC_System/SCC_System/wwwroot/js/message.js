﻿function info(message) {
    swal(message);
}

function success(message) {
    swal("សារជូនដំណឹង!", message, "success");
}


function warning(message) {
    swal("សារជូនដំណឹង!", message, "warning");
}

function AlertConfirm(message) {
    return swal({
        title: 'សារបញ្ជាក់',
        text: message,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'យល់ព្រម',
        cancelButtonText: 'បោះបង់',
        padding: '2em'
    }).then(function (result) {
        if (result.value) {
            return true;
        } else {
            return false;
        }
    });
}

