﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.ExchangeRate
{
    public class ExchangeRateBase : ComponentBase
    {
        [Parameter]
        public string? exchangeRateId { get; set; }

        [Inject]
        private NavigationManager? navigation { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        public ExchangeResponse? exResponse { get; set; }
        public List<TblExchangeRate>? lsexchangRate { get; set; }
        public TblExchangeRate exchangerate = new TblExchangeRate();
        public TblExchangeRate? editrate = new TblExchangeRate();
        public string name = "";
        protected async override Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }
            lsexchangRate = await exResponse.ListExchangeRate();
            StateHasChanged();
            if (exchangeRateId != null)
            {
                int id = int.Parse(exchangeRateId);
                editrate = await exResponse.getExchangeById(id);
            }

        }

        public void onNew()
        {
            navigation.NavigateTo("/newexchangerate");
        }
        public async void ToDiscount()
        {
            navigation.NavigateTo("/discount");
        }
        public async Task AddNewExchange()
        {

            if (exchangerate != null && exResponse != null)
            {
                if (!string.IsNullOrEmpty(exchangerate.CurrencyCode) && !string.IsNullOrEmpty(exchangerate.CurrencySymbol) &&
                    !string.IsNullOrEmpty(exchangerate.Rate.ToString()))
                {
                    bool res = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់បន្ថែមអត្រាប្ដូរប្រាក់ថ្មីមែនទេ?");
                    if (res)
                    {
                        await exResponse.AddExchangeRate(exchangerate);
                        await OnRemove();
                        StateHasChanged();
                        Message.Success(NotificationService, "ជោគជ័យ");

                    }
                }
            }
        }

        public async Task DeleteExchangeRate(int rateId)
        {
            await exResponse.deletedExchangeRate(rateId);
            lsexchangRate = await exResponse.ListExchangeRate();
            StateHasChanged();
        }

        public async Task OnEditExchange()
        {
            if (editrate != null)
            {
                if (!string.IsNullOrEmpty(editrate.CurrencyCode.ToString()) && !string.IsNullOrEmpty(editrate.CurrencySymbol) &&
                   !string.IsNullOrEmpty(editrate.Rate.ToString()))
                {
                    bool res = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែអត្រាប្ដូរប្រាក់​មែនទេ?");
                    if (res)
                    {
                        await exResponse.EditExchangeRate(editrate);
                        Message.Success(NotificationService, "ជោគជ័យ");
                        await OnRemove();
                        StateHasChanged();
                    }
                }
                else
                {
                    Message.Error(NotificationService, "មិនមានទិន្នន័យគ្រប់គ្រាន់");
                }

            }
        }

        public async Task OnRemove()
        {
            navigation.NavigateTo("/exchangerate");
            exchangerate = new TblExchangeRate();
            StateHasChanged();
        }

        public async void OnRefresh()
        {
            lsexchangRate = await exResponse.ListExchangeRate();
            StateHasChanged();
        }
    }
}
