﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.ExchangeRate
{
    public class ExchangeRateFormBase : ComponentBase
    {
        [Parameter]
        public string? exchangeRateId { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }

        [Inject]
        public ExchangeResponse? Data { get; set; }
        public TblExchangeRate? ExchangeRate { get; set; }
        public string name = "";

        [Inject]
        NavigationManager navigation { get; set; }
        [Inject]
        NotificationService notificationService { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }


        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }

            if (exchangeRateId != null)
            {
                int Id = int.Parse(exchangeRateId);
                ExchangeRate = await Data.getExchangeById(Id);
                //StateHasChanged();
            }
        }

        public async Task save()
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែមែនទេ?");
            if (result)
            {
                if (ExchangeRate != null)
                {
                    await Data.EditExchangeRate(ExchangeRate);
                    Message.Success(notificationService, "ជោគជ័យ");
                    navigation.NavigateTo("/exchangerate");
                    StateHasChanged();
                }
            }

        }

        public void onCancel()
        {
            navigation.NavigateTo("/exchangerate");
        }
    }

}
