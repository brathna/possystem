﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Product
{
    public class AddProductBase : ComponentBase
    {
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        public ProductResponse? Pro { get; set; }
        public TblProducts Product = new TblProducts();
        public List<TblCategory> Categories = new List<TblCategory>();
        public string name = "";
        [Inject]
        private NavigationManager navigation { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }

        public string? imageData;
        public async Task UploadImg(InputFileChangeEventArgs e)
        {
            var formart = "image/png";
            var resizeImage = await e.File.RequestImageFileAsync(formart, 300, 300);
            var buffer = new byte[resizeImage.Size];
            await resizeImage.OpenReadStream().ReadAsync(buffer);
            imageData = $"data:{formart};base64,{Convert.ToBase64String(buffer)}";

            var srt = imageData.Length;
            if (srt <= 200000)
            {
                Product.Url = imageData;
            }
            else
            {
                Message.Error(NotificationService, "រូបភាពមិនអាចធំជាង 2Mb");
            }
        }

        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }
            Categories = await Pro.GetCategoryByList();
        }

        public async Task AddNewProduct()
        {
            if (Product != null && Pro != null)
            {
                if (!string.IsNullOrEmpty(Product.ProductNameEn) && !string.IsNullOrEmpty(Product.ProductNameKh) &&
                    Product.CateId != 0 && Product.Price > 0)
                {
                    Product.Status = true;
                    await Pro.AddNewProduct(Product);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    await Pro.GetCategoryByList();
                    StateHasChanged();
                    navigation.NavigateTo("productlist");
                }

            }
        }
        public void onBtnExitNewProd()
        {
            navigation.NavigateTo("/productlist");
        }
    }
}