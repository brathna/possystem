﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.Product
{
    public class ProductListBase : ComponentBase
    {
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        NavigationManager Navigate { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        public ProductResponse? Pro { get; set; }
        public List<VwSelectAllProduct>? ProductList { get; set; }
        public bool isSearch = false;
        public string search;
        public string name = "";
        protected async override Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                Navigate.NavigateTo("/login");
            }

            ProductList = await Pro.ProductsList(isSearch, search);
        }
        public void onBtnNewImport()
        {
            //Navigate.NavigateTo("/import/infor");
            Navigate.NavigateTo("/stocklist");
        }
        public void onBtnAddProduct()
        {
            Navigate.NavigateTo("/product/info");
        }
        public void onBtnNewCategory()
        {
            Navigate.NavigateTo("/categorylist");
        }

        public async void onBtnRefresh()
        {
            ProductList = await Pro.ProductsList(false, "");
        }

        public async Task DeleteProduct(int id)
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់លុបមែនទេ?");
            if (result)
            {
                await Pro.DeleteProduct(id);
                ProductList = await Pro.ProductsList(false, "");
                StateHasChanged();
            }
        }

        public async Task onClickSearch()
        {
            isSearch = true;
            ProductList = await Pro.ProductsList(isSearch, search);
            StateHasChanged();
        }

    }
}
