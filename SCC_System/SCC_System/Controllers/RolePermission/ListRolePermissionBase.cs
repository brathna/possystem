﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.RolePermission
{
    public class ListRolePermissionBase : ComponentBase
    {
        [Inject]
        public IJSRuntime Runtime { get; set; }
        [Inject]
        private NavigationManager navigation { get; set; }
        [Inject]
        public UserResponse? userResponse { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        public RoleResponse? rolerespon { get; set; }
        public List<TblRole>? rolelist { get; set; }
        public List<VwGetRolePermission>? rolepermissions { get; set; }
        public List<TblPermission>? permissionlist { get; set; }
        public string name = "";
        //public List<TblSubPermission> listSub = new List<TblSubPermission>();

        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }

            //rolepermissions = rolepermissions.Where(f=>f.RolePerId==AuthenticationBase.rple)
            permissionlist = await rolerespon.GetPermissionList();
            rolelist = await userResponse.GetRoleByList();
            rolepermissions = await rolerespon.ListRolePermission();

            //this.listSub = await rolerespon.GetSubPremission();
        }
        public void onBtnNewRole()
        {
            navigation.NavigateTo("/adduserRole");
        }
        public async void onBtnRefresh()
        {
            rolepermissions = await rolerespon.ListRolePermission();
        }
        public async Task OnDeleteRole(int id)
        {
            bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់លុបមែនទេ?");
            if (result)
            {
                await rolerespon.DeleteRolePermission(id);
                rolepermissions = await rolerespon.ListRolePermission();
                StateHasChanged();
            }

        }
    }
}
