﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.RolePermission
{
    public class EditRolePermissionBase : ComponentBase
    {
        [Parameter]
        public string? roleperId { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        private NavigationManager navigation { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        public RoleResponse? rolerespon { get; set; }
        [Inject]
        public UserResponse? userrespon { get; set; }
        public TblRolePermission? rolepermission { get; set; }
        public List<TblRole>? roleList = new List<TblRole>();
        public List<TblPermission>? permissionList { get; set; }
        public string name = "";
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }

            permissionList = new List<TblPermission>();
            if (roleperId != null)
            {
                int id = int.Parse(roleperId);
                rolepermission = await rolerespon.GetRolePerById(id);
                roleList = await userrespon.GetRoleByList();
                permissionList = await rolerespon.GetPermissionList();
                StateHasChanged();
            }
        }

        public async Task OnEditUserRole()
        {

            if (rolepermission != null && rolerespon != null && userrespon != null)
            {
                if (rolepermission.RoleId > 0 && rolepermission.PerId > 0)
                {
                    bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែមែនទេ?");
                    if (result)
                    {
                        await rolerespon.EditRolePermission(rolepermission);
                        Message.Success(NotificationService, "ជោគជ័យ");
                        StateHasChanged();
                        navigation.NavigateTo("/userRole");
                    }
                }
                else
                {
                    Message.Error(NotificationService, "មិនមានទិន្នន័យគ្រប់គ្រាន់");
                }
            }
            //}
        }

        public void onBtnExitUserRole()
        {
            navigation.NavigateTo("/userRole");
        }
    }
}
