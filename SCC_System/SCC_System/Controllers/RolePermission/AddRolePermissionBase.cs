﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.RolePermission
{
    public class AddRolePermissionBase : ComponentBase
    {
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        private NavigationManager navigation { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        public RoleResponse? rolerespon { get; set; }
        [Inject]
        public UserResponse? userrespon { get; set; }
        public TblRolePermission rolepermission = new TblRolePermission();
        public List<TblRole> roleList = new List<TblRole>();
        public List<TblPermission> permissionList = new List<TblPermission>();
        public string name = "";
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }

            this.roleList = await this.userrespon.GetRoleByList();
            this.permissionList = await this.rolerespon.GetPermissionList();
        }

        public async Task OnNewRolePermission()
        {
            if (rolepermission != null && rolerespon != null && userrespon != null)
            {
                if (rolepermission.RoleId > 0 && rolepermission.PerId > 0)
                {
                    rolepermission.Status = true;
                    await rolerespon.AddRolePermission(rolepermission);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    await userrespon.GetRoleByList();
                    await rolerespon.GetPermissionList();
                    StateHasChanged();
                    navigation.NavigateTo("/userRole");
                }
            }
        }
        public void onBtnExitUserRole()
        {
            navigation.NavigateTo("/userRole");
        }

    }
}
