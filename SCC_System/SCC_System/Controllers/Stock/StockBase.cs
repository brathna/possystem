﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Stock
{
    public class StockBase : ComponentBase
    {
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        NavigationManager Navigate { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        public StockResponse? streposne { get; set; }
        public List<TblStock> lstStock = new List<TblStock>();
        public List<VwSelectStockOut> lstStockOut = new List<VwSelectStockOut>();
        public TblStock stock = new TblStock();
        public TblStockOut stockout = new TblStockOut();
        public string name = "";
        public int qty;
        public int stid;
        protected async override Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                Navigate.NavigateTo("/login");
            }
            lstStock = await this.streposne.ListStock();
            lstStockOut = await this.streposne.ListStouckOut();
        }
        public void onNewStcok()
        {
            Navigate.NavigateTo("/newstock");
        }
        public void onAddOutStcok()
        {
            Navigate.NavigateTo("/outstock");
        }
        public void ExitNewStock()
        {
            ClearAddStock();

        }
        public async void onBtnRefresh()
        {
            lstStock = await this.streposne.ListStock();
        }
        public async Task AddNewStock()
        {
            if (stock != null)
            {
                if (!string.IsNullOrEmpty(stock.ProNameKh) && !string.IsNullOrEmpty(stock.ProNameEn) && stock.Price > 0 && stock.Amount > 0
                    && stock.Qty > 0 && stock.ImportDate != null)
                {
                    string name = await localStorage.GetItemAsync<string>("user");
                    stock.UserId = int.Parse(name);
                    await streposne.AddNewStock(stock);
                    lstStock = await this.streposne.ListStock();
                    Message.Success(NotificationService, "ជោគជ័យ");
                    StateHasChanged();
                    ClearAddStock();

                }
                else
                {
                    Message.Error(NotificationService, "មិនមានទិន្នន័យគ្រប់គ្រាន់");

                }
            }
        }
        public async Task ClearAddStock()
        {
            stock.ProNameEn = "";
            stock.ProNameKh = "";
            stock.Qty = 0;
            stock.Unit = "";
            stock.Price = 0;
            stock.Amount = 0;
        }
        public async Task AddOutStock()
        {
            if (stockout != null)
            {
                if (!string.IsNullOrEmpty(stockout.Customer) && stockout.Qty > 0 && stockout.StOutDate != null && stockout.Stid > 0)
                {
                    if (lstStock.Count > 0)
                    {
                        foreach (var item in lstStock)
                        {
                            //get from index of Stid 
                            stid = stockout.Stid;
                            if (stid == item.StId)
                            {
                                qty = Convert.ToInt32(item.Qty - stockout.Qty);
                            }
                        }
                    }
                    await streposne.AddNewStockOut(stockout);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    StateHasChanged();
                    Navigate.NavigateTo("/stocklist");
                }
                else
                {
                    Message.Error(NotificationService, "មិនមានទិន្នន័យគ្រប់គ្រាន់");

                }
            }
        }

    }
}
