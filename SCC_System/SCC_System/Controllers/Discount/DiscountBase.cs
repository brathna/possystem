﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Discount
{
    public class DiscountBase : ComponentBase
    {
        [Parameter]
        public string? discountId { get; set; }

        [Inject]
        public DiscountResponse? dis_response { get; set; }
        public List<TblDiscount> disList = new List<TblDiscount>();
        public TblDiscount discount = new TblDiscount();
        public string name = "";

        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        NavigationManager navigate { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }

        protected async override Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigate.NavigateTo("/login");
            }

            this.disList = await this.dis_response.GetListDiscount();
            if (discountId != null)
            {
                int id = int.Parse(discountId);
                this.discount = await this.dis_response.GetDiscountById(id);
            }
        }

        public async void onBtnRefresh()
        {
            disList = await dis_response.GetListDiscount();
        }
        public async void ToAddNew()
        {
            navigate.NavigateTo("/adddiscount");
        }
        public async void onBtnExit()
        {
            navigate.NavigateTo("/discount");
        }
        public async Task AddNewDiscount()
        {
            if (discount != null && dis_response != null)
            {
                if (!string.IsNullOrEmpty(discount.DisPercent) && discount.Amount > 0)
                {
                    discount.Status = true;
                    await this.dis_response.NewDiscount(discount);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    StateHasChanged();
                    navigate.NavigateTo("discount");
                }
            }
        }

        public async Task DisableDiscount(int discId)
        {
            await dis_response.DeleteDiscount(discId);
            disList = await dis_response.GetListDiscount();
            StateHasChanged();
        }

        public async Task SaveUpdateDis()
        {
            if (discount != null && dis_response != null)
            {
                if (!string.IsNullOrEmpty(discount.DisPercent) && discount.Amount >= 0)
                {
                    bool res = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែការបញ្ចុះតម្លៃមែនទេ?");
                    if (res)
                    {
                        await this.dis_response.UpdateDiscount(discount);
                        Message.Success(NotificationService, "ជោគជ័យ");
                        StateHasChanged();
                        navigate.NavigateTo("/discount");
                    }
                }
                else
                {
                    Message.Error(NotificationService, "មិនមានទិន្នន័យគ្រប់គ្រាន់");
                }
            }

        }
    }
}
