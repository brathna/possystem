﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Employee
{
    public class EditEmployeeBase : ComponentBase
    {
        [Parameter]
        public string? employeeId { get; set; }
        [Inject]
        public ResponseData? Emp { get; set; }
        public TblEmployee? Employee { get; set; }
        public string name = "";

        [Inject]
        NavigationManager navigation { get; set; }
        [Inject]
        NotificationService notificationService { get; set; }
        [Inject]
        private IJSRuntime? Runtime { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }

        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }

            if (employeeId != null)
            {
                int Id = int.Parse(employeeId);
                Employee = await Emp.GetEmployeeById(Id);
                //StateHasChanged();
            }
        }

        //=============================================
        //Event call back for button update employee
        //=============================================
        public async Task SaveEmployee()
        {
            if (Employee != null && Emp != null)
            {
                if (!string.IsNullOrEmpty(Employee.FnameKh.Trim()) && !string.IsNullOrEmpty(Employee.LnameKh.Trim()) && !string.IsNullOrEmpty(Employee.FullNameEn.Trim())
                    && !string.IsNullOrEmpty(Employee.Gender.Trim()) && !string.IsNullOrEmpty(Employee.Position.Trim()) && Employee.Salary > 0)
                {
                    bool result = await Runtime.InvokeAsync<bool>("AlertConfirm", "តើអ្នកចង់កែប្រែមែនទេ?");
                    if (result)
                    {
                        await Emp.SaveEmployee(Employee);
                        Message.Success(notificationService, "ជោគជ័យ");
                        navigation.NavigateTo("/employee");
                        StateHasChanged();
                    }
                }
                else
                {
                    Message.Error(notificationService, "ទិន្នន័យមិនគ្រប់គ្រាន់");
                }
            }
        }

        public void onBtnExit()
        {
            navigation.NavigateTo("/employee");
        }
    }

}
