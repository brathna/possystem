﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Employee
{
    public class AddEmployeeBase : ComponentBase
    {

        [Inject]
        public ResponseData? Emp { get; set; }
        public TblEmployee Employee = new TblEmployee();
        public string name = "";

        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        NavigationManager navigation { get; set; }
        [Inject]
        NotificationService notificationService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }
        }

        public async Task CreateEmployee()
        {
            if (Employee != null && Emp != null)
            {
                //if (!string.IsNullOrEmpty(Employee.FnameKh.Trim()) && !string.IsNullOrEmpty(Employee.LnameKh.Trim()) && !string.IsNullOrEmpty(Employee.FullNameEn.Trim()) && Employee.Salary > 0)
                if (!string.IsNullOrEmpty(Employee.FnameKh) && !string.IsNullOrEmpty(Employee.LnameKh) && !string.IsNullOrEmpty(Employee.FullNameEn)
                    && !string.IsNullOrEmpty(Employee.Gender) && !string.IsNullOrEmpty(Employee.Position) && Employee.Salary > 0)
                {
                    Employee.Isactive = true;
                    await Emp.CreateEmployee(Employee);
                    Message.Success(notificationService, "ជោគជ័យ");
                    navigation.NavigateTo("/employee");
                }
                else
                {
                    Message.Error(notificationService, "ទិន្នន័យមិនគ្រប់គ្រាន់");
                }
            }

        }
        public void onBtnExitAddEmp()
        {
            navigation.NavigateTo("/employee");
        }

    }

}

