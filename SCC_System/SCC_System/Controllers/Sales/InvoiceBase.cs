﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.Sales
{
    public class InvoiceBase : ComponentBase
    {
        [Inject]
        public SaleResponse Sale { get; set; }
        public List<VwGetOrderById> getOrderDetail = new List<VwGetOrderById>();
        [Inject]
        public ExchangeResponse ex { get; set; }
        public List<TblExchangeRate> ExchangeRate = new List<TblExchangeRate>();
        public string cashierName { get; set; }
        public decimal Total { get; set; }
        public decimal Discount { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal CashRecieve { get; set; }
        public decimal PayBack { get; set; }
        public int OrderId { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
        public string size = "";
        public string name = "";
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        NavigationManager navigate { get; set; }
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigate.NavigateTo("/login");
            }
            this.ExchangeRate = await this.ex.ListExchangeRate();
            foreach (var item in ExchangeRate)
            {
                if (item.ExchangeDate != null)
                {
                    Rate = Convert.ToDecimal(item.Rate);
                }
            }
            this.getOrderDetail = await this.Sale.GetOrderlist();
            foreach (var item in getOrderDetail)
            {
                OrderId = item.OrderId;
                CashRecieve = Convert.ToDecimal(item.CashRecieve);
                GrandTotal = Convert.ToDecimal(item.GrandTotal);
                PayBack = Convert.ToDecimal(item.PayBack);
                Amount = Convert.ToDecimal(item.Amount);
                SubTotal = Convert.ToDecimal(item.SubTotal);
                size = item.Size;
            }
            this.cashierName = await localStorage.GetItemAsync<string>("username");
            StateHasChanged();
        }
    }
}
