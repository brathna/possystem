﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SCC_System.Controllers.Sales;
using SCC_System.Services;
using System.ComponentModel;

namespace SCC_System.Controllers.SaleReports
{
    public class SaleReportsBase : ComponentBase
    {
        [Inject]
        public SaleReportResponse salereport { get; set; }
        public List<sp_SaleReportResult> salereportresult = new List<sp_SaleReportResult>();
        public List<VwGetUserReport> getUserReport = new List<VwGetUserReport>();
        public SaleFilter salefilter = new SaleFilter();
      
        public string name = "";
        public bool isHide = false;
        [Inject] public IJSRuntime? JS { get; set; }
        //public SaleReportPrintBase saleReportPrintBase { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        NavigationManager navi { get; set; }
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navi.NavigateTo("/login");
            }
            await Filter();
            getUserReport = await this.salereport.GetUserReport();
        }

        public async Task DateFrom_Changed(DateTime? datefrom)
        {
            try
            {
                if (datefrom != null)
                {
                    salefilter.dateFrom = datefrom;
                }
                else
                {
                    salefilter.dateFrom = null;
                }
                await Filter();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task DateTo_Changed(DateTime? dateto)
        {
            try
            {
                if (dateto != null)
                {
                    salefilter.dateTo = dateto;
                }
                else
                {
                    salefilter.dateTo = null;
                }
                await Filter();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public async Task Saler_Changed(object saler)
        {
            try
            {
                int saleId = (int)(saler);
                if (saleId != 0)
                {
                    salefilter.SaleId = saleId;
                }
                else
                {
                    salefilter.SaleId = 0;
                }
                await Filter();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
     
        public async void OnPrint()
        {
            isHide = true;
            StateHasChanged();
            await JS.InvokeVoidAsync("initMyReportScript", "inv");
        }
        private async Task Filter()
        {
            salereportresult = await salereport.GetListSaleReport(salefilter);
            StateHasChanged();
        }


    }
}
