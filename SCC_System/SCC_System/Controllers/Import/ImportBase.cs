﻿using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SaleModal.Classes;
using SaleModal.DB.Model;
using SCC_System.Services;

namespace SCC_System.Controllers.Import
{
    public class ImportBase : ComponentBase
    {
        [Parameter]
        public string? importId { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }
        [Inject]
        private IJSRuntime Runtime { get; set; }
        [Inject]
        NavigationManager navigation { get; set; }

        [Inject]
        public ImportResponse import { get; set; }
        public List<VwGetImportById> listImport = new List<VwGetImportById>();
        public List<TblImportDetail> ImportDetails = new List<TblImportDetail>();
        //public TblImportDetail ImportDetails = new TblImportDetail();
        public ImportPost impPro = new ImportPost();
        public string name = "";
        protected override async Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                navigation.NavigateTo("/login");
            }
            //listImport = new List<VwGetImportById>();
            this.listImport = await this.import.ImportList();
            StateHasChanged();
        }
        //=============================================================

        public async Task OnNewImport()
        {
            if (impPro != null)
            {
                if (impPro.details != null)
                {
                    string name = await localStorage.GetItemAsync<string>("user");
                    impPro.UserId = int.Parse(name);
                    var result = await import.AddImport(impPro);
                    if (result.Code == 200 && result.Data != null)
                    {
                        //impPro = new ImportPost();
                        //ImportDetails = new List<TblImportDetail>();
                        StateHasChanged();
                        await Runtime.InvokeAsync<object>("warning", "ប្រតិបត្តិការជោគជ័យ​!");
                    }
                    else
                    {
                        await Runtime.InvokeAsync<object>("warning", result.Message);
                    }
                }
                else
                {
                    await Runtime.InvokeAsync<object>("warning", "Faill​!");
                }
            }
        }

        //============================================================
        //onBtn to new Pages 

        public void onBtnNew()
        {
            navigation.NavigateTo("/newimport");
        }
        public async void onBtnRefresh()
        {
            this.listImport = await this.import.ImportList();
        }

        public void onBtnExitImport()
        {
            navigation.NavigateTo("/import/infor");
        }

    }
}
