﻿
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Radzen;
using SaleModal.DB.Model;
using SCC_System.Services;
using SCC_System.Utilities;

namespace SCC_System.Controllers.Category
{
    public class AddCategoryBase : ComponentBase
    {
        [Inject]
        public CategoryResponse? CategoryResponse { get; set; }
        public TblCategory NewCategory = new TblCategory();
        public string name = "";

        [Inject]
        NavigationManager Navigation { get; set; }
        [Inject]
        NotificationService NotificationService { get; set; }
        [Inject]
        ILocalStorageService localStorage { get; set; }

        protected async override Task OnInitializedAsync()
        {
            name = await localStorage.GetItemAsync<string>("username");
            if (name == "" || name == null)
            {
                Navigation.NavigateTo("/login");
            }
        }

        public async Task AddNewCategory()
        {
            if (NewCategory != null && CategoryResponse != null)
            {
                if (!string.IsNullOrEmpty(NewCategory.CategoryNameEn) && !string.IsNullOrEmpty(NewCategory.CategoryNameKh))
                {
                    NewCategory.Status = true;
                    await CategoryResponse.CreateNewCategory(NewCategory);
                    Message.Success(NotificationService, "ជោគជ័យ");
                    StateHasChanged();
                    Navigation.NavigateTo("/categorylist");
                }
            }
        }
        public void onBtnExitCategory()
        {
            Navigation.NavigateTo("/categorylist");
        }
    }
}
