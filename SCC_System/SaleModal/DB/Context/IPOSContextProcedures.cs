﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using SaleModal.DB.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace SaleModal.DB.Context
{
    public partial interface IPOSContextProcedures
    {
        Task<List<sp_SaleReportResult>> sp_SaleReportAsync(DateTime? date_from, DateTime? date_to, int? saler, OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default);
        Task<int> sp_update_discountAsync(OutputParameter<int> returnValue = null, CancellationToken cancellationToken = default);
    }
}
