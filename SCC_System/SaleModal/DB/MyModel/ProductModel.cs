﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaleModal.DB.MyModel
{
    public class ProductModel
    {
        public string name { get; set; } = null;
        public int ProId { get; set; }
        public string CateId { get; set; }
        public string ProductNameEn { get; set; }
        public string ProductNameKh { get; set; }
        public decimal? Price { get; set; }
        public int? Type { get; set; }
        public int? Qty { get; set; }
        public bool? Status { get; set; }
        public string Url { get; set; }
    }
}
