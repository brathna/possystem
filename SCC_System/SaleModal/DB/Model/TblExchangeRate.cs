﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace SaleModal.DB.Model
{
    public partial class TblExchangeRate
    {
        public int CurrencyId { get; set; }
        public string CurrencyCode { get; set; }
        public string CurrencySymbol { get; set; }
        public decimal? Rate { get; set; }
        public DateTime? ExchangeDate { get; set; }
        public bool? RateActive { get; set; }
    }
}