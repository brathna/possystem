﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
#nullable disable
using System;
using System.Collections.Generic;

namespace SaleModal.DB.Model
{
    public partial class TblOrder
    {
        public int OrderId { get; set; }
        public int? UserId { get; set; }
        public DateTime? OrderDate { get; set; }
        public decimal? GrandTotal { get; set; }
        public decimal? CashRecieve { get; set; }
        public decimal? PayBack { get; set; }
        public decimal? SubTotal { get; set; }
        public int? DisId { get; set; }
        public decimal? Amount { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? Rate { get; set; }
    }
}