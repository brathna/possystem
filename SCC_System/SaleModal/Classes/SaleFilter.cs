﻿namespace SaleModal.Classes
{
    //=========use for sale report===========//
    public class SaleFilter
    {
        public DateTime? dateFrom { get; set; } = DateTime.Now;
        public DateTime? dateTo { get; set; } = DateTime.Now;
        public int SaleId { get; set; } = 0;
    }
}
