﻿namespace SaleModal.Classes
{
    public class SaleDetail
    {
        public int? ProId { get; set; }
        public string ProNameEn { get; set; }
        public string ProNameKh { get; set; }
        public int? Qty { get; set; }
        public decimal? Price { get; set; }
        public decimal? Total { get; set; }
        //public int? DisId { get; set; }
        //public decimal? AfterDiscount { get; set; }
    }
}
