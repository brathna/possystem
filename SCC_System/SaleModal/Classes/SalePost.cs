﻿using SaleModal.DB.Model;

namespace SaleModal.Classes
{
    //=============use for sale produt=================//
    public class SalePost
    {
        public int? UserId { get; set; }
        public decimal? GrandTotal { get; set; }
        public decimal? CashRecieve { get; set; }
        public string? Size { get; set; }
        public decimal? PayBack { get; set; }
        public DateTime? OrderDate { get; set; }
        public decimal? SubTotal { get; set; }
        public int? DisId { get; set; }
        public decimal? Amount { get; set; }
        public int? CurrencyId { get; set; }
        public decimal? Rate { get; set; }
        public List<TblOrderDetail> details { get; set; }
        //public int? DisId { get; set; }
        //public int? CurrencyId { get; set; }
        //public decimal? Amount { get; set; }

        //public decimal? Rate { get; set; }

    }
}
