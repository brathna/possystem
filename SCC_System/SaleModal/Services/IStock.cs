﻿using SaleModal.Classes;
using SaleModal.DB.Model;

namespace SaleModal.Services
{
    public interface IStock
    {
        public Task<ApiResponse> CreateStock(TblStock st);
        public Task<ApiResponse> ListStock();
        public Task<ApiResponse> ListStockOut();
        public Task<ApiResponse> OutStock(TblStockOut stout);
    }
}
