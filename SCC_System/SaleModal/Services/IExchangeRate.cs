﻿using SaleModal.Classes;
using SaleModal.DB.Model;

namespace SaleModal.Services
{
    public interface IExchangeRate
    {
        public Task<ApiResponse> ListExchangeRate();
        public Task<ApiResponse> CreateExchangeRate(TblExchangeRate exchange);
        public Task<ApiResponse> UpdateExchangeRate(TblExchangeRate exchangerate);
        public Task<ApiResponse> GetExchangeRateByDate();
        public Task<ApiResponse> getExchangeById(int id);
        public Task<ApiResponse> DeleteRate(int rateId);
    }
}
