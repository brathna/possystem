﻿using SaleModal.Classes;
using SaleModal.DB.Model;

namespace SaleModal.Services
{
    public interface IRolePermission
    {
        public Task<ApiResponse> ListRolePermission();
        public Task<ApiResponse> CreateRolePermission(TblRolePermission roleper);
        public Task<ApiResponse> UpdateRolePermission(TblRolePermission rolePermission);
        public Task<ApiResponse> DeleteRolePermission(int id);
        public Task<ApiResponse> GetRolePerById(int id);
        public Task<ApiResponse> GetPermByList();
        public Task<ApiResponse> GetSubPermissionList();
    }
}
