﻿using SaleModal.Classes;

namespace SaleModal.Services
{
    public interface ISaleReport
    {
        public Task<ApiResponse> ListSaleReport(SaleFilter filter);
        public Task<ApiResponse> GetUserReport();
    }
}
