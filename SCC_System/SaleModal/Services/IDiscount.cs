﻿using SaleModal.Classes;
using SaleModal.DB.Model;

namespace SaleModal.Services
{
    public interface IDiscount
    {
        public Task<ApiResponse> ListDiscount();
        public Task<ApiResponse> CreateDiscount(TblDiscount cre_dis);
        public Task<ApiResponse> UpdateDiscount(TblDiscount up_dis);
        public Task<ApiResponse> GetDiscountById(int disId);
        public Task<ApiResponse> DeleteDiscount(int de_disId);
    }
}
